#!/bin/bash
#
#
BUS=$(lsusb | grep -i google | grep -o "[0-9][0-9][0-9]" | head -n1)
DEVICE=$(lsusb | grep -i google | grep -o "[0-9][0-9][0-9]" | tail -n1)

if [ -z "$BUS" ]
then
	BUS=$(lsusb | grep -i apple | grep -o "[0-9][0-9][0-9]" | tail -n1)
	DEVICE=$(lsusb | grep -i apple | grep -o "[0-9][0-9][0-9]" | tail -n1)
	if [ -z "$BUS" ]
	then
		echo "NO DEVICE FOUND"
		exit 1
	else
		echo "Apple Device Confirmed"
	fi
else
	echo "Android Device Confirmed"
fi

echo "Bus: "$BUS
echo "Device: "$DEVICE
cd /tmp/
#wget https://content-api.nowsecure.com/sha256/1fb8818c1d382610dbc845acd54ef54d7822e3452c2f0bd80125fcd299364775 && mv 1fb8818c1d382610dbc845acd54ef54d7822e3452c2f0bd80125fcd299364775 usbreset && chmod +x usbreset
usbreset /dev/bus/usb/$BUS/$DEVICE
sleep 10
echo "Checking ADB..."
adb devices
echo "Checking fastboot..."
fastboot devices
echo "No go away."